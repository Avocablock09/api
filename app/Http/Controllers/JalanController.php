<?php

namespace App\Http\Controllers;

use App\Models\jalan;
use App\Http\Requests\StorejalanRequest;
use App\Http\Requests\UpdatejalanRequest;

class JalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorejalanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorejalanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jalan  $jalan
     * @return \Illuminate\Http\Response
     */
    public function show(jalan $jalan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jalan  $jalan
     * @return \Illuminate\Http\Response
     */
    public function edit(jalan $jalan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatejalanRequest  $request
     * @param  \App\Models\jalan  $jalan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatejalanRequest $request, jalan $jalan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jalan  $jalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(jalan $jalan)
    {
        //
    }
}
